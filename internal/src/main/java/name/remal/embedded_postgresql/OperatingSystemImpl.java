package name.remal.embedded_postgresql;

import kr.motd.maven.os.Detector;
import org.immutables.metainf.Metainf.Service;
import org.jetbrains.annotations.NotNull;
import oshi.SystemInfo;
import oshi.software.os.OSProcess;

import java.util.Optional;
import java.util.Properties;

import static java.util.Collections.emptyList;
import static java.util.Objects.requireNonNull;
import static kr.motd.maven.os.Detector.*;

@Service(OperatingSystem.class)
public class OperatingSystemImpl implements OperatingSystem {

    private static final String NAME;

    private static final String ARCH;

    private static final String CLASSIFIER;

    static {
        Properties props = new DetectorImpl().detect();
        NAME = requireNonNull(props.getProperty(DETECTED_NAME), DETECTED_NAME);
        ARCH = requireNonNull(props.getProperty(DETECTED_ARCH), DETECTED_ARCH);
        CLASSIFIER = requireNonNull(props.getProperty(DETECTED_CLASSIFIER), DETECTED_CLASSIFIER);
    }

    private static class DetectorImpl extends Detector {

        @NotNull
        public Properties detect() {
            Properties props = new Properties();
            super.detect(props, emptyList());
            return props;
        }

        @Override
        protected void log(String message) {
        }

        @Override
        protected void logProperty(String name, String value) {
        }

    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getArch() {
        return ARCH;
    }

    @Override
    @NotNull
    public String getClassifier() {
        return CLASSIFIER;
    }

    private static final SystemInfo SYSTEM_INFO = new SystemInfo();

    @Override
    public long getTotalMemory() {
        return SYSTEM_INFO.getHardware().getMemory().getTotal();
    }

    @Override
    public int getPid() {
        return SYSTEM_INFO.getOperatingSystem().getProcessId();
    }

    @Override
    public boolean isAlive(int pid) {
        return null != SYSTEM_INFO.getOperatingSystem().getProcess(pid);
    }

    @Override
    @NotNull
    public Optional<String> getProcessPath(int pid) {
        OSProcess osProcess = SYSTEM_INFO.getOperatingSystem().getProcess(pid);
        if (null == osProcess) return Optional.empty();
        return Optional.of(osProcess.getPath());
    }

}

package name.remal.embedded_postgresql;

import com.google.common.base.Joiner;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.Objects;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static name.remal.embedded_postgresql.AtomicFileOperations.createNewFile;
import static name.remal.embedded_postgresql.AtomicFileOperations.modifyTextFile;
import static name.remal.embedded_postgresql.PostgresqlPidFileParser.parsePostgresqlPidFileFromDataDir;

public class MonitoringApp {

    private static final OperatingSystem OS = new OperatingSystemImpl();

    public static void main(@NotNull String[] args) {
        setupPidFile(new File(args[0]).getAbsoluteFile());
        start(new File(args[1]).getAbsoluteFile());
    }

    private static void setupPidFile(@NotNull File pidFile) {
        createNewFile(pidFile);
        modifyTextFile(pidFile, text -> {
            text = text.trim();
            if (!text.isEmpty()) {
                String[] lines = text.split("[\\r\\n]+");
                int pid = Integer.parseInt(lines[0]);
                OS.getProcessPath(pid).ifPresent(processPath -> {
                    if (Objects.equals(processPath, lines[1])) {
                        System.exit(0);
                    }
                });
            }

            return OS.getPid() + "\n" + OS.getProcessPath();
        });
    }

    @SneakyThrows
    private static void start(@NotNull File storeFile) {
        while (true) {
            createNewFile(storeFile);
            modifyTextFile(storeFile, null, text -> {
                return Joiner.on("\n").join(Stream.of(text.split("[\\r\\n]+"))
                    .map(String::trim)
                    .filter(it -> !it.isEmpty())
                    .parallel()
                    .map(dataDirPath -> {
                        File dataDir = new File(dataDirPath);
                        PostgresqlPidFileInfo pidFileInfo = parsePostgresqlPidFileFromDataDir(dataDir);
                        if (null != pidFileInfo && !isPostgresqlProcess(pidFileInfo.getPid())) return pidFileInfo;
                        return null;
                    })
                    .filter(Objects::nonNull)
                    .collect(toList()));
            });

            Thread.sleep(5000);
        }
    }

    private static boolean isPostgresqlProcess(int pid) {
        String processPath = OS.getProcessPath(pid).orElse(null);
        if (null == processPath) return false;
        return processPath.contains("postgres");
    }

}

package name.remal.embedded_postgresql.gradle.pg_binaries;

import org.jetbrains.annotations.NotNull;

import static java.util.Objects.requireNonNull;

public enum OS {

    linux,
    windows;

    @NotNull
    public static OS fromString(@NotNull String osStr) {
        requireNonNull(osStr, "str");
        try {
            return OS.valueOf(osStr);
        } catch (IllegalArgumentException e) {
            for (OS item : OS.values()) {
                if (item.name().equalsIgnoreCase(osStr)) {
                    return item;
                }
            }
            throw e;
        }
    }

}

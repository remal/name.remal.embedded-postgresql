package name.remal.embedded_postgresql.gradle.pg_binaries;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;

import static java.util.Objects.requireNonNull;

@RequiredArgsConstructor
@EqualsAndHashCode
public class Classifier {

    @NotNull
    public final OS os;

    @NotNull
    public final Arch arch;

    @Override
    @NotNull
    public String toString() {
        return os + "-" + arch;
    }

    @NotNull
    public static Classifier fromString(@NotNull String classifierStr) {
        requireNonNull(classifierStr, "classifierStr");
        int delimPos = classifierStr.indexOf("-");
        if (delimPos <= 0) throw new IllegalArgumentException("Illegal classifier format. Should be <os>-<arch>.");
        return new Classifier(OS.fromString(classifierStr.substring(0, delimPos)), Arch.fromString(classifierStr.substring(delimPos + 1)));
    }

}

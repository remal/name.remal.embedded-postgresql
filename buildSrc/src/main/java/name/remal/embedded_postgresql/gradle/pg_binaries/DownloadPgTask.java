package name.remal.embedded_postgresql.gradle.pg_binaries;

import lombok.Setter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import name.remal.embedded_postgresql.gradle.PgTask;
import name.remal.embedded_postgresql.gradle.Utils;
import org.codehaus.groovy.runtime.IOGroovyMethods;
import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.CacheableTask;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.OutputFile;
import org.gradle.api.tasks.TaskAction;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import static java.lang.Math.toIntExact;
import static java.lang.String.format;
import static java.nio.file.Files.*;
import static java.nio.file.StandardCopyOption.ATOMIC_MOVE;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

@Slf4j
@CacheableTask
public class DownloadPgTask extends DefaultTask implements PgTask {

    @Setter
    private Classifier classifier;

    {
        onlyIf(task -> !((DownloadPgTask) task).getArchiveFile().isFile());
    }

    @Input
    @NotNull
    @SneakyThrows
    public URL getURL() {
        Classifier classifier = this.classifier;
        if (Arch.x86_64 == classifier.arch) {
            return new URL(format("https://get.enterprisedb.com/postgresql/postgresql-%s-1-%s-x64-binaries.%s", getPostgresVersion(), classifier.os, getFileExtension()));
        } else if (Arch.x86_32 == classifier.arch) {
            return new URL(format("https://get.enterprisedb.com/postgresql/postgresql-%s-1-%s-binaries.%s", getPostgresVersion(), classifier.os, getFileExtension()));
        } else {
            throw new IllegalStateException("Unsupported arch: " + classifier.arch);
        }
    }

    @NotNull
    @OutputFile
    public File getArchiveFile() {
        return new File(getBuildCacheDir(), format("downloaded/%s.%s", classifier, getFileExtension()));
    }

    @TaskAction
    @SneakyThrows
    protected void executeDownload() {
        if (getProject().getGradle().getStartParameter().isOffline()) throw new IllegalStateException("Unable to download in offline mode");

        File archiveFile = getArchiveFile();
        createDirectories(archiveFile.getParentFile().toPath());

        URL url = getURL();
        log.info("Downloading {}", url);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setConnectTimeout(toIntExact(Utils.CONNECTION_TIMEOUT.toMillis()));
        connection.setReadTimeout(toIntExact(Utils.CONNECTION_TIMEOUT.toMillis()));

        {
            int responseCode = connection.getResponseCode();
            if (200 != responseCode) throw new IllegalStateException(format("Error downloading %s: status %d", url, responseCode));
        }

        try (InputStream inputStream = connection.getInputStream()) {
            File tmpFile = new File(archiveFile.getParentFile(), archiveFile.getName() + ".downloading");
            deleteIfExists(tmpFile.toPath());
            try (OutputStream outputStream = new FileOutputStream(tmpFile)) {
                IOGroovyMethods.leftShift(outputStream, inputStream);
            }

            deleteIfExists(archiveFile.toPath());
            move(tmpFile.toPath(), archiveFile.toPath(), REPLACE_EXISTING, ATOMIC_MOVE);

        } finally {
            connection.disconnect();
        }

        setDidWork(true);
    }

    @NotNull
    private String getFileExtension() {
        return OS.linux == classifier.os ? "tar.gz" : "zip";
    }

}

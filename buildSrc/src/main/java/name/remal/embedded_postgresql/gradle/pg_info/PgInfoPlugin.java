package name.remal.embedded_postgresql.gradle.pg_info;

import name.remal.embedded_postgresql.gradle.PgProjectPlugin;
import org.gradle.api.Project;
import org.jetbrains.annotations.NotNull;

public class PgInfoPlugin implements PgProjectPlugin {

    @Override
    public void apply(@NotNull Project project) {
        project.getExtensions().create("embeddedPostgresql", EmbeddedPostgresqlExtension.class);
    }

}

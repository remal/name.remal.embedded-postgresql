package name.remal.embedded_postgresql.gradle.pg_info;

import name.remal.embedded_postgresql.gradle.pg_binaries.Classifier;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedHashSet;
import java.util.Objects;
import java.util.stream.StreamSupport;

import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toCollection;

public class EmbeddedPostgresqlExtension {

    @NotNull
    private ClassifiersCollection classifiers = new ClassifiersCollection();

    @NotNull
    public ClassifiersCollection getClassifiers() {
        return classifiers;
    }

    public void setClassifiers(@NotNull Iterable<Classifier> classifiers) {
        requireNonNull(classifiers, "classifiers");
        this.classifiers = StreamSupport.stream(classifiers.spliterator(), false)
            .filter(Objects::nonNull)
            .collect(toCollection(ClassifiersCollection::new));
    }

    public static class ClassifiersCollection extends LinkedHashSet<Classifier> {

        public boolean add(@NotNull String str) {
            return add(Classifier.fromString(str));
        }

    }

}

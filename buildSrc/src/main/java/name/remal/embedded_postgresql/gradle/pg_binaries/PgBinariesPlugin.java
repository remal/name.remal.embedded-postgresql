package name.remal.embedded_postgresql.gradle.pg_binaries;

import lombok.extern.slf4j.Slf4j;
import name.remal.building.gradle_plugins.SimpleBuildCacheExtension;
import name.remal.building.gradle_plugins.SimpleBuildCachePlugin;
import name.remal.embedded_postgresql.gradle.PgProjectPlugin;
import name.remal.embedded_postgresql.gradle.pg_info.EmbeddedPostgresqlExtension;
import name.remal.embedded_postgresql.gradle.pg_info.PgInfoPlugin;
import org.gradle.api.Project;
import org.gradle.api.plugins.JavaPluginConvention;
import org.gradle.api.tasks.AbstractCopyTask;
import org.gradle.api.tasks.SourceSetContainer;
import org.gradle.api.tasks.bundling.AbstractArchiveTask;
import org.gradle.api.tasks.bundling.Jar;
import org.gradle.api.tasks.bundling.Zip;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.regex.Pattern;

import static java.lang.String.format;
import static name.remal.embedded_postgresql.gradle.PgProjectPlugin.getPostgresVersion;
import static org.gradle.api.artifacts.Dependency.ARCHIVES_CONFIGURATION;
import static org.gradle.api.plugins.JavaPlugin.JAR_TASK_NAME;
import static org.gradle.api.plugins.JavaPlugin.PROCESS_RESOURCES_TASK_NAME;

@Slf4j
public class PgBinariesPlugin implements PgProjectPlugin {

    @Override
    public void apply(@NotNull Project project) {
        project.getPluginManager().apply(PgInfoPlugin.class);

        project.getPluginManager().apply(SimpleBuildCachePlugin.class);
        project.getExtensions().getByType(SimpleBuildCacheExtension.class).setVersion(project.property(POSTGRES_VERSION_PROPERTY_NAME) + "-" + project.property("build-cache.version"));

        project.getPluginManager().withPlugin("java", __ -> {
            SourceSetContainer sourceSets = project.getConvention().getPlugin(JavaPluginConvention.class).getSourceSets();

            EmbeddedPostgresqlExtension embeddedPostgresqlExtension = project.getExtensions().getByType(EmbeddedPostgresqlExtension.class);
            Collection<Classifier> classifiers = new LinkedHashSet<>(embeddedPostgresqlExtension.getClassifiers());
            for (Classifier classifier : classifiers) {
                DownloadPgTask downloadTask = createDownloadTask(project, classifier);
                AbstractArchiveTask repackTask = createRepackTask(project, classifier, downloadTask);

                project.getTasks().withType(AbstractCopyTask.class).getByName(PROCESS_RESOURCES_TASK_NAME, task -> {
                    task.dependsOn(repackTask);
                    task.from(getRepackedDir(project));
                });

                Jar originalJarTask = project.getTasks().withType(Jar.class).getByName(JAR_TASK_NAME);
                Jar jarTask = project.getTasks().create(
                    getPgTaskName(originalJarTask.getName(), classifier),
                    Jar.class,
                    task -> {
                        task.setClassifier(classifier.toString());

                        task.dependsOn(originalJarTask);
                        task.setManifest(originalJarTask.getManifest());
                        task.setMetadataCharset(originalJarTask.getMetadataCharset());
                        task.setZip64(originalJarTask.isZip64());

                        task.from(project.zipTree(originalJarTask.getArchivePath()));
                        for (Classifier excludingClassifier : classifiers) {
                            if (excludingClassifier.equals(classifier)) continue;
                            task.exclude(format("META-INF/pgsql/%s.*", excludingClassifier));
                            task.exclude(format("META-INF/pgsql/%s-*", excludingClassifier));
                        }
                    }
                );
                project.getArtifacts().add(ARCHIVES_CONFIGURATION, jarTask);
            }
        });
    }

    @NotNull
    private DownloadPgTask createDownloadTask(@NotNull Project project, @NotNull Classifier classifier) {
        return project.getTasks().create(
            getPgTaskName("postgresqlDownload", classifier),
            DownloadPgTask.class,
            task -> {
                task.setClassifier(classifier);
            }
        );
    }

    @NotNull
    private File getRepackedDir(@NotNull Project project) {
        File buildCacheDir = project.getExtensions().getByType(SimpleBuildCacheExtension.class).getDir();
        return new File(buildCacheDir, "repacked");
    }

    @NotNull
    private AbstractArchiveTask createRepackTask(@NotNull Project project, @NotNull Classifier classifier, @NotNull DownloadPgTask downloadTask) {
        ExtractPgTask extractTask = project.getTasks().create(
            getPgTaskName("postgresqlExtract", classifier),
            ExtractPgTask.class,
            task -> {
                task.dependsOn(downloadTask);
                task.setClassifier(classifier);
                task.setArchiveFile(downloadTask.getArchiveFile());
            }
        );

        return project.getTasks().create(
            getPgTaskName("postgresqlRepack", classifier),
            Zip.class,
            task -> {
                task.dependsOn(extractTask);
                task.from(project.fileTree(extractTask.getBinariesDir()));
                task.setDestinationDir(new File(getRepackedDir(project), "META-INF/pgsql"));
                task.setBaseName(classifier.toString());
                task.setVersion(getPostgresVersion(project));
                task.setClassifier(null);
                task.onlyIf(__ -> !task.getArchivePath().isFile());
            }
        );
    }

    @NotNull
    private static String getPgTaskName(@NotNull String baseName, @NotNull Classifier classifier) {
        String name = baseName + "_" + classifier;
        name = TASK_NAME_ESCAPER.matcher(name).replaceAll("_");
        return name;
    }

    private static final Pattern TASK_NAME_ESCAPER = Pattern.compile("[^\\w]");

}

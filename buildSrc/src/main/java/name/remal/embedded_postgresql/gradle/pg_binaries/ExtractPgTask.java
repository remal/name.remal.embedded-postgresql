package name.remal.embedded_postgresql.gradle.pg_binaries;

import lombok.Setter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import name.remal.embedded_postgresql.gradle.PgTask;
import name.remal.embedded_postgresql.gradle.Utils;
import org.codehaus.groovy.runtime.IOGroovyMethods;
import org.gradle.api.Action;
import org.gradle.api.DefaultTask;
import org.gradle.api.file.FileTree;
import org.gradle.api.file.FileVisitDetails;
import org.gradle.api.tasks.CacheableTask;
import org.gradle.api.tasks.Input;
import org.gradle.api.tasks.OutputDirectory;
import org.gradle.api.tasks.TaskAction;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.regex.Pattern;

import static java.lang.String.format;
import static java.nio.file.Files.createDirectories;
import static java.nio.file.Files.move;
import static java.nio.file.StandardCopyOption.ATOMIC_MOVE;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static java.util.regex.Pattern.CASE_INSENSITIVE;

@Slf4j
@CacheableTask
public class ExtractPgTask extends DefaultTask implements PgTask {

    @Setter
    private Classifier classifier;

    @Setter
    @Input
    private File archiveFile;

    {
        onlyIf(task -> !((ExtractPgTask) task).getBinariesDir().isDirectory());
    }

    @NotNull
    @OutputDirectory
    public File getBinariesDir() {
        return new File(getBuildCacheDir(), format("extracted/%s", classifier));
    }

    private static final Pattern EXCLUDING_PATTERN = Pattern.compile("^(doc/|include/|symbols/|pgAdmin.*/|StackBuilder.*/|bin/(stackbuilder.*)|share/man/)", CASE_INSENSITIVE);

    @TaskAction
    @SneakyThrows
    protected void doUnpack() {
        log.info("Extracting {}", archiveFile);

        final FileTree fileTree;
        String archiveFileName = archiveFile.getName();
        if (archiveFileName.endsWith(".tar.gz")) {
            fileTree = getProject().tarTree(getProject().getResources().gzip(archiveFile));
        } else if (archiveFileName.endsWith(".zip")) {
            fileTree = getProject().zipTree(archiveFile);
        } else {
            throw new IllegalStateException("Unsupported archive format: " + archiveFileName);
        }

        File binariesDir = getBinariesDir();
        File tmpDir = new File(binariesDir.getParentFile(), binariesDir.getName() + ".extracting");
        Utils.deleteRecursive(tmpDir.toPath());
        fileTree.visit(new Action<FileVisitDetails>() {
            @Override
            @SneakyThrows
            public void execute(@NotNull FileVisitDetails details) {
                if (details.isDirectory()) return;

                String path = details.getPath();
                {
                    String prefix = "pgsql/";
                    if (path.startsWith(prefix)) path = path.substring(prefix.length());
                }

                if (EXCLUDING_PATTERN.matcher(path).find()) return;

                File targetFile = new File(tmpDir, path);
                createDirectories(targetFile.getParentFile().toPath());
                try (InputStream inputStream = details.open(); OutputStream outputStream = new FileOutputStream(targetFile)) {
                    IOGroovyMethods.leftShift(outputStream, inputStream);
                }
            }
        });

        Utils.deleteRecursive(binariesDir.toPath());
        move(tmpDir.toPath(), binariesDir.toPath(), REPLACE_EXISTING, ATOMIC_MOVE);

        setDidWork(true);
    }

}

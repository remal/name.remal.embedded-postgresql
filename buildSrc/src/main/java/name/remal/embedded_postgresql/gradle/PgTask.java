package name.remal.embedded_postgresql.gradle;

import name.remal.building.gradle_plugins.SimpleBuildCacheExtension;
import org.gradle.api.Task;
import org.jetbrains.annotations.NotNull;

import java.io.File;

public interface PgTask extends Task {

    @NotNull
    default String getPostgresVersion() {
        return PgProjectPlugin.getPostgresVersion(getProject());
    }

    @NotNull
    default File getBuildCacheDir() {
        return getProject().getExtensions().getByType(SimpleBuildCacheExtension.class).getDir();
    }

}

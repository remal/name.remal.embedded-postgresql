package name.remal.embedded_postgresql.gradle;

import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.jetbrains.annotations.NotNull;

public interface PgProjectPlugin extends Plugin<Project> {

    @Override
    void apply(@NotNull Project project);

    String POSTGRES_VERSION_PROPERTY_NAME = "postgresql.version";

    @NotNull
    static String getPostgresVersion(@NotNull Project project) {
        Object propValue = project.property(POSTGRES_VERSION_PROPERTY_NAME);
        if (null == propValue) throw new IllegalStateException("Project property " + POSTGRES_VERSION_PROPERTY_NAME + " is not set");
        String version = propValue.toString();
        if (version.isEmpty()) throw new IllegalStateException("Project property " + POSTGRES_VERSION_PROPERTY_NAME + " is empty");
        return version;
    }

}

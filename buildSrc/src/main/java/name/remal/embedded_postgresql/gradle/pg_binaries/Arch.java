package name.remal.embedded_postgresql.gradle.pg_binaries;

import org.jetbrains.annotations.NotNull;

import static java.util.Objects.requireNonNull;

public enum Arch {

    x86_64,
    x86_32;

    @NotNull
    public static Arch fromString(@NotNull String archStr) {
        requireNonNull(archStr, "archStr");
        try {
            return Arch.valueOf(archStr);
        } catch (IllegalArgumentException e) {
            for (Arch item : Arch.values()) {
                if (item.name().equalsIgnoreCase(archStr)) {
                    return item;
                }
            }
            throw e;
        }
    }

}

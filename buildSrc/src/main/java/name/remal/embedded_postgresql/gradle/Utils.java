package name.remal.embedded_postgresql.gradle;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.Duration;

import static java.net.URLDecoder.decode;
import static java.net.URLEncoder.encode;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.file.FileVisitResult.CONTINUE;
import static java.nio.file.Files.*;

public interface Utils {

    Charset CHARSET = UTF_8;

    Duration CONNECTION_TIMEOUT = Duration.ofSeconds(10);

    @SneakyThrows
    static void deleteRecursive(@NotNull Path path) {
        if (!exists(path)) return;
        walkFileTree(path, new SimpleFileVisitor<Path>() {
            @NotNull
            @Override
            public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) throws IOException {
                delete(file);
                return CONTINUE;
            }

            @NotNull
            @Override
            public FileVisitResult postVisitDirectory(final Path dir, @Nullable final IOException e) throws IOException {
                if (null != e) throw e;
                delete(dir);
                return CONTINUE;
            }
        });
    }

    @NotNull
    static String getExtension(@NotNull File file) {
        String name = file.getName();
        int dotPos = name.lastIndexOf('.');
        return 0 <= dotPos ? name.substring(dotPos + 1) : "";
    }

    @NotNull
    @SneakyThrows
    static String decodeURIComponent(@NotNull String uriComponent) {
        return decode(uriComponent, CHARSET.name());
    }

    @NotNull
    @SneakyThrows
    static String encodeURIComponent(@NotNull String uriComponent) {
        return encode(uriComponent, CHARSET.name());
    }

}

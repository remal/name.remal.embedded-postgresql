package name.remal.embedded_postgresql;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import static java.util.Arrays.asList;
import static name.remal.embedded_postgresql.StorageType.UnsupportedHostMessageHolder.UNSUPPORTED_HOST_MESSAGE;
import static name.remal.embedded_postgresql.Utils.isEmpty;
import static name.remal.embedded_postgresql.Utils.isNotEmpty;

enum StorageType {

    DEFAULT(""), RAM_DRIVE("ramdrive", "ram-drive");

    @NotNull
    private final Set<String> uriHosts;

    StorageType(@NotNull String... uriHosts) {
        this.uriHosts = new LinkedHashSet<>(asList(uriHosts));
    }

    @NotNull
    public static StorageType fromURIHost(@Nullable String host) {
        if (null == host) host = "";
        for (StorageType storageType : StorageType.values()) {
            if (storageType.uriHosts.contains(host)) {
                return storageType;
            }
        }

        throw new IllegalArgumentException(UNSUPPORTED_HOST_MESSAGE);
    }

    protected static final class UnsupportedHostMessageHolder {

        @NotNull
        public static final String UNSUPPORTED_HOST_MESSAGE;

        static {
            List<String> hosts = new ArrayList<>();
            for (StorageType storageType : StorageType.values()) {
                storageType.uriHosts.forEach(host -> {
                    if (isNotEmpty(host)) hosts.add(host);
                });
            }

            if (isEmpty(hosts)) {
                UNSUPPORTED_HOST_MESSAGE = "Unsupported host";

            } else {
                StringBuilder sb = new StringBuilder();
                for (String host : hosts) {
                    if (isEmpty(sb)) {
                        sb.append("Unsupported host. Can be empty or ");
                    } else {
                        sb.append(", ");
                    }
                    sb.append("'").append(host).append("'");
                }
                sb.append('.');
                UNSUPPORTED_HOST_MESSAGE = sb.toString();
            }
        }
    }

}

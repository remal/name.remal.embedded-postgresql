package name.remal.embedded_postgresql;

import lombok.SneakyThrows;
import org.immutables.metainf.Metainf.Service;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.*;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.function.Function;

import static java.sql.DriverManager.registerDriver;
import static name.remal.embedded_postgresql.DatabaseUtils.escapeObjectName;
import static name.remal.embedded_postgresql.PostgresqlDriverUtils.processProperties;
import static name.remal.embedded_postgresql.Utils.defaultString;
import static org.postgresql.PGProperty.PASSWORD;
import static org.postgresql.PGProperty.USER;

@Service(Driver.class)
public class EmbeddedPostgresqlDriver implements Driver {

    static {
        try {
            registerDriver(new EmbeddedPostgresqlDriver());
        } catch (SQLException e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    protected final org.postgresql.Driver pgDriver = new org.postgresql.Driver();

    @Override
    public boolean acceptsURL(@NotNull String url) throws SQLException {
        return null != ConnectionParams.parse(url);
    }

    private static final ConcurrentMap<String, Boolean> EXISTED_USERS = new ConcurrentHashMap<>();

    private static final ConcurrentMap<String, Boolean> EXISTED_DATABASES = new ConcurrentHashMap<>();

    @Nullable
    @Override
    public Connection connect(@NotNull String url, @Nullable Properties props) throws SQLException {
        ConnectionParams connectionParams = ConnectionParams.parse(url);
        if (null == connectionParams) return null;

        EmbeddedPostgresqlManager manager = new EmbeddedPostgresqlManager(connectionParams);

        props = processProperties(props);
        String user = defaultString(USER.get(props));
        USER.set(props, user);
        String password = defaultString(PASSWORD.get(props));
        PASSWORD.set(props, password);
        EXISTED_USERS.computeIfAbsent(user, __ -> manager.createUserIfNotExists(user, password));

        EXISTED_DATABASES.computeIfAbsent(connectionParams.getDatabaseName(), databaseName -> {
            return manager.forAdminConnection(new Function<Connection, Boolean>() {
                @Override
                @SneakyThrows
                public Boolean apply(Connection connection) {
                    try (PreparedStatement stmt = connection.prepareStatement(
                        "CREATE DATABASE " + escapeObjectName(databaseName) + " WITH"
                            + " OWNER = " + escapeObjectName(user)
                    )) {
                        stmt.execute();
                        return true;
                    } catch (SQLException sqlException) {
                        if ("42P04".equals(sqlException.getSQLState())) {
                            return false;
                        }
                        throw sqlException;
                    }
                }
            });
        });

        return pgDriver.connect(manager.createJDBCConnectionURL(), props);
    }

    @NotNull
    @Override
    public DriverPropertyInfo[] getPropertyInfo(@NotNull String url, @Nullable Properties props) throws SQLException {
        ConnectionParams connectionParams = ConnectionParams.parse(url);
        if (null == connectionParams) return new DriverPropertyInfo[0];

        EmbeddedPostgresqlManager manager = new EmbeddedPostgresqlManager(connectionParams);
        String jdbcConnectionURL = manager.createJDBCConnectionURL();
        return pgDriver.getPropertyInfo(jdbcConnectionURL, processProperties(props));
    }

    @Override
    public int getMajorVersion() {
        return pgDriver.getMajorVersion();
    }

    @Override
    public int getMinorVersion() {
        return pgDriver.getMinorVersion();
    }

    @Override
    public boolean jdbcCompliant() {
        return pgDriver.jdbcCompliant();
    }

    @NotNull
    @Override
    public java.util.logging.Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return pgDriver.getParentLogger();
    }

}

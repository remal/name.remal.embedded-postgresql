package name.remal.embedded_postgresql;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

import static java.lang.String.format;
import static name.remal.embedded_postgresql.Utils.defaultString;
import static name.remal.embedded_postgresql.Utils.encodeURIComponent;

public class EmbeddedPostgresqlInfo {

    @NotNull
    private final ConnectionParams connectionParams;

    private final int port;

    public EmbeddedPostgresqlInfo(@NotNull ConnectionParams connectionParams, int port) {
        this.connectionParams = connectionParams;
        this.port = port;
    }

    @NotNull
    public ConnectionParams getConnectionParams() {
        return connectionParams;
    }

    public int getPort() {
        return port;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof EmbeddedPostgresqlInfo)) return false;
        EmbeddedPostgresqlInfo that = (EmbeddedPostgresqlInfo) obj;
        if (!Objects.equals(getConnectionParams(), that.getConnectionParams())) return false;
        if (!Objects.equals(getPort(), that.getPort())) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = 31 * result + Objects.hashCode(getConnectionParams());
        result = 31 * result + Integer.hashCode(getPort());
        return result;
    }

    @Override
    public String toString() {
        return EmbeddedPostgresqlInfo.class.getSimpleName()
            + '{'
            + "connectionParams=" + connectionParams
            + ", port=" + port
            + '}';
    }

    @NotNull
    public String getJDBCConnectionURL() {
        ConnectionParams params = getConnectionParams();
        return format(
            "jdbc:postgresql://localhost:%d/%s?%s",
            getPort(),
            encodeURIComponent(params.getDatabaseName()),
            defaultString(params.getUriQuery())
        );
    }

}

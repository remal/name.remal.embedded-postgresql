package name.remal.embedded_postgresql;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.postgresql.Driver;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Optional;
import java.util.Properties;
import java.util.function.Consumer;
import java.util.function.Function;

import static java.lang.Math.max;
import static java.lang.Math.min;
import static java.lang.String.format;
import static name.remal.embedded_postgresql.AtomicFileOperations.*;
import static name.remal.embedded_postgresql.BinariesExtractor.extractBinariesTo;
import static name.remal.embedded_postgresql.BuildInfo.PROJECT_ID;
import static name.remal.embedded_postgresql.Config.*;
import static name.remal.embedded_postgresql.Constants.*;
import static name.remal.embedded_postgresql.DatabaseUtils.escapeObjectName;
import static name.remal.embedded_postgresql.FS.*;
import static name.remal.embedded_postgresql.Logger.logDebug;
import static name.remal.embedded_postgresql.Logger.logInfo;
import static name.remal.embedded_postgresql.Password.*;
import static name.remal.embedded_postgresql.PostgresqlDriverUtils.processProperties;
import static name.remal.embedded_postgresql.PostgresqlPidFileParser.PID_FILE_RELATIVE_PATH;
import static name.remal.embedded_postgresql.PostgresqlPidFileParser.parsePostgresqlPidFileFromDataDir;
import static name.remal.embedded_postgresql.Utils.encodeURIComponent;
import static name.remal.embedded_postgresql.Utils.executeCommand;
import static name.remal.public_data.KnownSocketPorts.isKnownTCPPort;
import static org.apache.commons.codec.CharEncoding.UTF_8;
import static org.apache.commons.codec.digest.DigestUtils.md5Hex;
import static org.postgresql.PGProperty.*;

public class EmbeddedPostgresqlManager {

    public static final String ADMIN_USER = Constants.ADMIN_USER;

    public static final String ADMIN_DATABASE = Constants.ADMIN_DATABASE;

    @NotNull
    private final ConnectionParams connectionParams;

    @NotNull
    private final StorageType storageType;

    @NotNull
    private final File dataDir;

    public EmbeddedPostgresqlManager(@NotNull ConnectionParams connectionParams) {
        this.connectionParams = connectionParams;
        this.storageType = connectionParams.getStorageType();

        if (StorageType.DEFAULT == storageType) {
            dataDir = file(DATA_DIR, encodeURIComponent(connectionParams.getNamespace()));
        } else if (StorageType.RAM_DRIVE == storageType) {
            dataDir = ramDriveFile(PROJECT_ID, DATA_RELATIVE_PATH, encodeURIComponent(connectionParams.getNamespace()));
        } else {
            throw new UnsupportedOperationException("Unsupported " + StorageType.class.getSimpleName() + ": " + storageType);
        }
    }

    @NotNull
    public EmbeddedPostgresqlInfo getInfo() {
        PostgresqlPidFileInfo pidFileInfo = parsePostgresqlPidFileFromDataDir(dataDir);
        if (null == pidFileInfo) {
            start();
            pidFileInfo = parsePostgresqlPidFileFromDataDir(dataDir);
        }
        if (null == pidFileInfo) throw new IllegalStateException(PID_FILE_RELATIVE_PATH + " can't be parsed");

        return new EmbeddedPostgresqlInfo(connectionParams, pidFileInfo.getPort());
    }

    @NotNull
    public String createJDBCConnectionURL() {
        return getInfo().getJDBCConnectionURL();
    }

    @SneakyThrows
    public void forAdminConnection(@NotNull Consumer<Connection> action) {
        start();
        try (Connection connection = tryToOpenAdminConnection()) {
            action.accept(connection);
        }
    }

    @SneakyThrows
    public <T> T forAdminConnection(@NotNull Function<Connection, T> action) {
        start();
        try (Connection connection = tryToOpenAdminConnection()) {
            return action.apply(connection);
        }
    }

    @SneakyThrows
    public boolean createUserIfNotExists(@NotNull String user, @NotNull String password) {
        if (null != readUserPassword(user)) return false;
        start();
        try (Connection connection = tryToOpenAdminConnection()) {
            try (PreparedStatement stmt = connection.prepareStatement("CREATE ROLE " + escapeObjectName(user) + " WITH"
                + " ENCRYPTED PASSWORD 'md5" + md5Hex((password + user).getBytes(UTF_8)) + "'"
                + " NOSUPERUSER CREATEDB CREATEROLE INHERIT LOGIN")) {
                stmt.execute();
            }
            writeUserPassword(user, password);
            return true;

        } catch (SQLException sqlException) {
            if ("42710".equals(sqlException.getSQLState())) {
                return false;
            }
            throw sqlException;
        }
    }

    private volatile boolean isStarted = false;

    @SneakyThrows
    private void start() {
        if (isStarted) return;
        synchronized (this) {
            if (isStarted) return;

            initdb();

            synchronizedWithTempFile(file(dataDir, DATA_PROJECT_RELATIVE_PATH, "start.lock"), () -> {
                int port = 0;
                {
                    Integer registryPort = DatabasesRegistry.readPort(dataDir);
                    if (null != registryPort) port = registryPort;
                }
                if (port <= 0) {
                    port = getFreePort();
                }

                int attemptsCounter = 0;
                while (true) {
                    ++attemptsCounter;

                    if (isPostgresProcessExists()) return;

                    logInfo("Staring Postgresql on port {}", port);
                    writeConfig(port);

                    try {
                        execPgCmd("pg_ctl", "start"
                            , "-w"
                            , "-D", dataDir.getAbsolutePath()
                        );

                    } catch (CommandExecutingException executingException) {
                        String executionOutput = executingException.getOutputText();
                        if (null != executionOutput && attemptsCounter <= 100) {
                            executionOutput = executionOutput.replace("\t", " ").replace("\r", "");
                            while (executionOutput.contains("  ")) executionOutput = executionOutput.replace("  ", " ");
                            if (executionOutput.contains("FATAL: could not create any TCP/IP sockets")) {
                                logInfo("Port {} is bound, making another start attempt", port);
                                port = getFreePort();
                                continue;
                            }
                        }
                        throw executingException;
                    }

                    DatabasesRegistry.store(dataDir, port);

                    logInfo("Postgresql successfully started on port {}", port);
                    return;
                }
            });

            {
                int attemptsCounter = 0;
                while (true) {
                    ++attemptsCounter;
                    try {
                        checkAdminConnection();
                        break;
                    } catch (Exception exception) {
                        //noinspection ConstantConditions
                        if (exception instanceof SQLException) {
                            if (attemptsCounter <= 1000) {
                                Thread.sleep(50);
                                continue;
                            }
                        }
                        throw exception;
                    }
                }
            }

            isStarted = true;
        }
    }

    private boolean isPostgresProcessExists() {
        return Optional.ofNullable(parsePostgresqlPidFileFromDataDir(dataDir))
            .map(PostgresqlPidFileInfo::getPid)
            .flatMap(OS::getProcessPath)
            .orElse("")
            .contains("postgres");
    }

    @SneakyThrows
    private static int getFreePort() {
        for (int port = 32767; 1024 <= port; --port) {
            if (isKnownTCPPort(port)) continue;
            try (ServerSocket socket = new ServerSocket(port)) {
                return socket.getLocalPort();
            } catch (IOException e) {
                // do nothing
            }
        }
        throw new IOException("Free port was not found");
    }

    @NotNull
    private File getWrittenConfigFlagFile() {
        return file(dataDir, DATA_PROJECT_RELATIVE_PATH, "config-was-generated.flag");
    }

    private boolean wasConfigWritten() {
        return getWrittenConfigFlagFile().isFile();
    }

    private void writeConfig(int port) {
        Config config = new Config();

        if (StorageType.RAM_DRIVE == storageType) {
            config.set("fsync", "off");
            config.set("full_page_writes", "off");
        }

        config.set("listen_addresses", "'localhost'");
        config.set("port", port);

        config.set("superuser_reserved_connections", 5);
        int maxConnections = max(50, min(10 * Runtime.getRuntime().availableProcessors(), 500));
        config.set("max_connections", maxConnections);
        config.set("autovacuum", "on");

        // Next values are computed using PgTune algorithm for Desktop application: http://pgtune.leopard.in.ua/
        long totalMemory = OS.getTotalMemory();
        long sharedBuffersBytes = Math.max(totalMemory / 16, 128 * BYTES_IN_KILOBYTE);
        if (OS.isWindows()) sharedBuffersBytes = Math.min(sharedBuffersBytes, 512 * BYTES_IN_MEGABYTE);
        config.setBytes("shared_buffers", sharedBuffersBytes);
        config.setBytes("effective_cache_size", totalMemory / 4);
        config.setBytes("work_mem", (totalMemory - sharedBuffersBytes) / (maxConnections * 3 * 6));
        config.setBytes("maintenance_work_mem", Math.min(totalMemory / 16, 2 * BYTES_IN_GIGABYTE));
        config.set("checkpoint_completion_target", "0.5");
        config.setBytes("min_wal_size", 100 * BYTES_IN_MEGABYTE);
        config.setBytes("max_wal_size", 200 * BYTES_IN_MEGABYTE);
        long walBuffersBytes = Math.max(3 * sharedBuffersBytes / 100, 16 * BYTES_IN_MEGABYTE);
        if (14 * BYTES_IN_MEGABYTE <= walBuffersBytes) walBuffersBytes = 16 * BYTES_IN_MEGABYTE;
        config.setBytes("wal_buffers", walBuffersBytes);
        config.set("default_statistics_target", 100);

        config.store(new File(dataDir, "postgresql.conf"));

        createNewFile(getWrittenConfigFlagFile());
    }

    @NotNull
    private Config readConfig() {
        Config config = new Config();
        config.parse(new File(dataDir, "postgresql.conf"));
        return config;
    }

    @SneakyThrows
    private void checkAdminConnection() {
        try (Connection connection = tryToOpenAdminConnection()) {
            // do nothing
        }
    }

    @SneakyThrows
    private Connection tryToOpenAdminConnection() {
        PostgresqlPidFileInfo pidFileInfo = parsePostgresqlPidFileFromDataDir(dataDir);
        if (null == pidFileInfo) throw new IllegalStateException(PID_FILE_RELATIVE_PATH + " can't be parsed");
        String jdbcURL = format("jdbc:postgresql://localhost:%d/%s", pidFileInfo.getPort(), ADMIN_DATABASE);

        String user = ADMIN_USER;
        String password = readUserPassword(user);
        if (null == password) throw new IllegalStateException("Password can't be read for user " + user);

        Properties props = new Properties();
        props.put(USER.getName(), user);
        props.put(PASSWORD.getName(), password);
        props.put(APPLICATION_NAME.getName(), PROJECT_ID + ".admin");
        return new Driver().connect(jdbcURL, processProperties(props));
    }

    private volatile boolean isDatabaseInitialized = false;

    private void initdb() {
        if (isDatabaseInitialized) return;
        synchronized (this) {
            if (isDatabaseInitialized) return;
            boolean isDirNew = createDirIfNotExists(dataDir, dir -> {
                logDebug("Initializing Postgresql data dir into {}", dir);

                File passwordFile = newTempFile("password");
                try {
                    String password = createPassword();
                    writePassword(password, passwordFile);
                    execPgCmd("initdb"
                        , "--encoding=UTF-8"
                        , "--no-locale"
                        , "--auth=md5"
                        , "--username=" + ADMIN_USER
                        , "--pwfile=" + passwordFile.getAbsolutePath()
                        , dir.getAbsolutePath()
                    );

                    writeUserPassword(ADMIN_USER, password, dir);

                } finally {
                    passwordFile.delete();
                }

                logDebug("Moving Postgresql data dir from {} to {}", dir, dataDir);
            });
            if (!isDirNew) logDebug("Postgresql data dir was already created in {}", dataDir);
            isDatabaseInitialized = true;
        }
    }

    private void writeUserPassword(@NotNull String user, @NotNull String password) {
        writeUserPassword(user, password, dataDir);
    }

    @Nullable
    private String readUserPassword(@NotNull String user) {
        return readUserPassword(user, dataDir);
    }

    private static void writeUserPassword(@NotNull String user, @NotNull String password, @NotNull File dataDir) {
        writePassword(password, getUserPasswordFile(user, dataDir));
    }

    @Nullable
    private static String readUserPassword(@NotNull String user, @NotNull File dataDir) {
        return readPassword(getUserPasswordFile(user, dataDir));
    }

    @NotNull
    private static File getUserPasswordFile(@NotNull String user, @NotNull File dataDir) {
        return file(dataDir, DATA_PROJECT_RELATIVE_PATH, "passwords", encodeURIComponent(user));
    }

    private static String execPgCmd(@NotNull String executable, @NotNull String... args) {
        extractBinaries();
        File executableFile = file(BINARIES_DIR, "bin", executable);
        return executeCommand(executableFile.getAbsolutePath(), args);
    }

    private static volatile boolean areBinariesExtracted = false;

    private static void extractBinaries() {
        if (areBinariesExtracted) return;
        synchronized (EmbeddedPostgresqlManager.class) {
            if (areBinariesExtracted) return;
            extractBinariesTo(BINARIES_DIR);
            areBinariesExtracted = true;
        }
    }

}

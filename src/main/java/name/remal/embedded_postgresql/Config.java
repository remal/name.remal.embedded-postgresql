package name.remal.embedded_postgresql;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.TreeMap;

import static name.remal.embedded_postgresql.AtomicFileOperations.readTextFile;
import static name.remal.embedded_postgresql.AtomicFileOperations.writeTextFile;
import static name.remal.embedded_postgresql.Logger.logDebug;

class Config {

    private final Map<String, String> values = new TreeMap<>();

    @SneakyThrows
    public void parse(@NotNull File configFile) {
        String text = readTextFile(configFile);
        try (Scanner scanner = new Scanner(text)) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();

                {
                    int commentPos = line.indexOf('#');
                    if (0 <= commentPos) line = line.substring(0, commentPos);
                }

                line = line.trim();
                if (line.isEmpty()) continue;

                int assignmentPos = line.indexOf('=');
                if (assignmentPos < 0) continue;

                String key = line.substring(0, assignmentPos).trim();
                if (key.isEmpty()) continue;

                String value = line.substring(assignmentPos + 1).trim();

                values.put(key, value);
            }
        }
    }

    @SneakyThrows
    public void store(@NotNull File configFile) {
        logDebug("Writing Postgresql config {}", configFile);
        StringBuilder sb = new StringBuilder();
        for (Entry<String, String> entry : values.entrySet()) {
            sb.append(entry.getKey()).append(" = ").append(entry.getValue()).append("\n");
        }
        writeTextFile(configFile, sb.toString());
    }

    @Nullable
    public String get(@NotNull String key) {
        return values.get(key);
    }

    public void set(@NotNull String key, @Nullable Object value) {
        if (null != value) {
            values.put(key, value.toString());
        } else {
            values.remove(key);
        }
    }

    public static final long BYTES_IN_KILOBYTE = 1024;

    public static final long BYTES_IN_MEGABYTE = 1024 * BYTES_IN_KILOBYTE;

    public static final long BYTES_IN_GIGABYTE = 1024 * BYTES_IN_MEGABYTE;

    public static final long BYTES_IN_TERABYTE = 1024 * BYTES_IN_GIGABYTE;

    public void setBytes(@NotNull String key, long bytes) {
        if (0 == bytes) {
            set(key, "0kB");

        } else if (0 == bytes % BYTES_IN_TERABYTE || BYTES_IN_TERABYTE <= bytes / 256) {
            set(key, (bytes / BYTES_IN_TERABYTE) + "TB");
        } else if (0 == bytes % BYTES_IN_GIGABYTE || BYTES_IN_GIGABYTE <= bytes / 256) {
            set(key, (bytes / BYTES_IN_GIGABYTE) + "GB");
        } else if (0 == bytes % BYTES_IN_MEGABYTE || BYTES_IN_MEGABYTE <= bytes / 256) {
            set(key, (bytes / BYTES_IN_MEGABYTE) + "MB");
        } else {
            set(key, (bytes / BYTES_IN_KILOBYTE) + "kB");
        }
    }

    public void remove(@NotNull String key) {
        values.remove(key);
    }

    @Override
    public String toString() {
        return values.toString();
    }

}

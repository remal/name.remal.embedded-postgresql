package name.remal.embedded_postgresql;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Objects;

import static name.remal.embedded_postgresql.Utils.isEmpty;

public class ConnectionParams {

    private static final String SCHEME = "embedded-postgresql";

    @Nullable
    @Contract("null->null")
    public static ConnectionParams parse(@Nullable URI jdbcURI) {
        if (null == jdbcURI) return null;
        if (!Objects.equals("jdbc", jdbcURI.getScheme())) return null;
        final URI uri;
        {
            String schemeSpecificPart = jdbcURI.getRawSchemeSpecificPart();
            if (isEmpty(schemeSpecificPart)) return null;
            try {
                uri = new URI(schemeSpecificPart);
            } catch (URISyntaxException ignored) {
                return null;
            }
        }
        if (!Objects.equals(SCHEME, uri.getScheme())) return null;
        if (-1 != uri.getPort()) return null;

        StorageType storageType = StorageType.fromURIHost(uri.getHost());

        final String namespace;
        final String databaseName;
        {
            String path = uri.getRawPath();
            while (path.startsWith("/")) path = path.substring(1);
            int delimPos = path.indexOf('/');
            if (delimPos < 0) return null;
            namespace = path.substring(0, delimPos);
            if (isEmpty(namespace) || namespace.contains("/")) return null;
            databaseName = path.substring(delimPos + 1);
            if (isEmpty(databaseName) || databaseName.contains("/")) return null;
        }

        return new ConnectionParams(
            storageType,
            namespace,
            databaseName,
            uri.getRawQuery()
        );
    }

    @Nullable
    @Contract("null->null")
    public static ConnectionParams parse(@Nullable String jdbcURI) {
        if (null == jdbcURI) return null;
        if (!jdbcURI.startsWith("jdbc:" + SCHEME + ":")) return null;
        final URI uri;
        try {
            uri = new URI(jdbcURI);
        } catch (URISyntaxException ignored) {
            return null;
        }
        return parse(uri);
    }

    @NotNull
    private final StorageType storageType;

    @NotNull
    private final String namespace;

    @NotNull
    private final String databaseName;

    @Nullable
    private final String uriQuery;

    public ConnectionParams(@NotNull StorageType storageType, @NotNull String namespace, @NotNull String databaseName, @Nullable String uriQuery) {
        this.storageType = storageType;
        this.namespace = namespace;
        this.databaseName = databaseName;
        this.uriQuery = uriQuery;
    }

    @NotNull
    public StorageType getStorageType() {
        return storageType;
    }

    @NotNull
    public String getNamespace() {
        return namespace;
    }

    @NotNull
    public String getDatabaseName() {
        return databaseName;
    }

    @Nullable
    public String getUriQuery() {
        return uriQuery;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof ConnectionParams)) return false;
        ConnectionParams that = (ConnectionParams) obj;
        if (!Objects.equals(getStorageType(), that.getStorageType())) return false;
        if (!Objects.equals(getNamespace(), that.getNamespace())) return false;
        if (!Objects.equals(getDatabaseName(), that.getDatabaseName())) return false;
        if (!Objects.equals(getUriQuery(), that.getUriQuery())) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = 31 * result + Objects.hashCode(getStorageType());
        result = 31 * result + Objects.hashCode(getNamespace());
        result = 31 * result + Objects.hashCode(getDatabaseName());
        result = 31 * result + Objects.hashCode(getUriQuery());
        return result;
    }

    @Override
    @NotNull
    public String toString() {
        return ConnectionParams.class.getSimpleName()
            + '{'
            + "storageType=" + storageType
            + ", namespace='" + namespace + '\''
            + ", databaseName='" + databaseName + '\''
            + ", uriQuery='" + uriQuery + '\''
            + '}';
    }

}

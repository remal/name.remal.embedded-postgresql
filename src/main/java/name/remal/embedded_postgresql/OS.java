package name.remal.embedded_postgresql;

import org.jetbrains.annotations.NotNull;

import java.util.Optional;
import java.util.ServiceLoader;

abstract class OS {

    private static final OperatingSystem OPERATING_SYSTEM = ServiceLoader.load(OperatingSystem.class, PackedDependenciesClassLoader.getInstance()).iterator().next();

    public static boolean isWindows() {
        return OPERATING_SYSTEM.isWindows();
    }

    @NotNull
    public static String getName() {
        return OPERATING_SYSTEM.getName();
    }

    @NotNull
    public static String getArch() {
        return OPERATING_SYSTEM.getArch();
    }

    @NotNull
    public static String getClassifier() {
        return OPERATING_SYSTEM.getClassifier();
    }

    public static long getTotalMemory() {
        return OPERATING_SYSTEM.getTotalMemory();
    }

    @NotNull
    public static Optional<String> getProcessPath(int pid) {
        return OPERATING_SYSTEM.getProcessPath(pid);
    }

    private OS() {
    }

}

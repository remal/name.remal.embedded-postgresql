package name.remal.embedded_postgresql;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static com.google.common.io.ByteStreams.copy;
import static java.lang.String.format;
import static java.nio.file.Files.createDirectories;
import static name.remal.embedded_postgresql.AtomicFileOperations.createDirIfNotExists;
import static name.remal.embedded_postgresql.BuildInfo.POSTGRESQL_VERSION;
import static name.remal.embedded_postgresql.Logger.logDebug;

interface BinariesExtractor {

    @SneakyThrows
    static void extractBinariesTo(@NotNull File dir) {
        boolean isDirNew = createDirIfNotExists(dir, tempDir -> {
            logDebug("Extracting Postgresql binaries to {}", tempDir);
            String resourceName = format("META-INF/pgsql/%s-%s.zip", OS.getClassifier(), POSTGRESQL_VERSION);
            try (InputStream inputStream = BinariesExtractor.class.getClassLoader().getResourceAsStream(resourceName)) {
                if (null == inputStream) throw new IllegalStateException("Embedded Postgresql resource can't be found: " + resourceName);
                try (ZipInputStream zipInputStream = new ZipInputStream(inputStream)) {
                    while (true) {
                        ZipEntry zipEntry = zipInputStream.getNextEntry();
                        if (null == zipEntry) break;
                        if (zipEntry.isDirectory()) continue;

                        String relativeName = zipEntry.getName();
                        while ('/' == relativeName.charAt(0) || '\\' == relativeName.charAt(0)) {
                            relativeName = relativeName.substring(1);
                        }

                        File targetFile = new File(tempDir, relativeName);
                        createDirectories(targetFile.getParentFile().toPath());
                        try (OutputStream outputStream = new FileOutputStream(targetFile)) {
                            copy(zipInputStream, outputStream);
                        }

                        if (relativeName.startsWith("bin/")) {
                            targetFile.setExecutable(true);
                        }
                    }
                }
            }

            logDebug("Moving Postgresql binaries from {} to {}", tempDir, dir);
        });
        if (!isDirNew) logDebug("Postgresql binaries were already extracted to {}", dir);
    }

}

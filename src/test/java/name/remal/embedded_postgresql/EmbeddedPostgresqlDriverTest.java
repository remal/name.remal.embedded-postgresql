package name.remal.embedded_postgresql;

import org.testng.annotations.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import static java.lang.System.currentTimeMillis;
import static name.remal.embedded_postgresql.BuildInfo.PROJECT_ID;

@Test
public class EmbeddedPostgresqlDriverTest {

    @Test(enabled = false)
    public void test() throws Throwable {
        try (Connection connection = DriverManager.getConnection("jdbc:embedded-postgresql://ramdrive/" + PROJECT_ID + "/test-" + currentTimeMillis(), String.valueOf(currentTimeMillis()), String.valueOf(currentTimeMillis()))) {
            try (PreparedStatement stmt = connection.prepareStatement("SELECT 1")) {
                stmt.execute();
            }
        }
    }

}

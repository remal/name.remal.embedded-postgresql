package name.remal.embedded_postgresql;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.security.SecureRandom;
import java.util.Random;

import static java.lang.Long.toHexString;
import static name.remal.embedded_postgresql.AtomicFileOperations.readTextFile;
import static name.remal.embedded_postgresql.AtomicFileOperations.writeTextFile;

interface Password {

    @NotNull
    static String createPassword() {
        long password;
        Random random = new SecureRandom();
        do {
            password = random.nextLong();
        } while (password <= Integer.MAX_VALUE);
        return toHexString(password);
    }

    @SneakyThrows
    static void writePassword(@NotNull String password, @NotNull File file) {
        writeTextFile(file, password);
    }

    @Nullable
    @SneakyThrows
    static String readPassword(@NotNull File file) {
        file = file.getAbsoluteFile();
        if (!file.isFile()) return null;
        return readTextFile(file);
    }

}

package name.remal.embedded_postgresql;

import org.jetbrains.annotations.Nullable;

interface Lambda {

    interface UncheckedRunnable {
        void run() throws Throwable;
    }

    interface UncheckedSupplier<T> {
        @Nullable
        T get() throws Throwable;
    }

    interface UncheckedConsumer<T> {
        void accept(T t) throws Throwable;
    }

    interface UncheckedBiConsumer<T, U> {
        void accept(T t, U u) throws Throwable;
    }

    interface UncheckedFunction<T, R> {
        @Nullable
        R apply(T t) throws Throwable;
    }

    interface UncheckedBiFunction<T, U, R> {
        @Nullable
        R apply(T t, U u) throws Throwable;
    }

}

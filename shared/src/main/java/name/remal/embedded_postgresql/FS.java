package name.remal.embedded_postgresql;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;

import static name.remal.embedded_postgresql.BuildInfo.PROJECT_ID;
import static name.remal.embedded_postgresql.Utils.isEmpty;

interface FS {

    @NotNull
    @SneakyThrows
    static File newTempFile(@Nullable String prefix, @Nullable String suffix) {
        prefix = PROJECT_ID + "." + (null != prefix ? prefix + '.' : "");
        suffix = (null != suffix ? '.' + suffix : "") + ".temp";
        return File.createTempFile(prefix, suffix).getAbsoluteFile();
    }

    @NotNull
    static File newTempFile(@Nullable String prefix) {
        return newTempFile(prefix, null);
    }

    @NotNull
    static File newTempFile() {
        return newTempFile(null);
    }

    @NotNull
    static File file(@NotNull File file, @NotNull String... subPaths) {
        File result = file;
        for (String subPath : subPaths) {
            result = new File(result.getAbsolutePath(), subPath);
        }
        return result.getAbsoluteFile();
    }

    @NotNull
    static File file(@NotNull String path, @NotNull String... subPaths) {
        File result = new File(path);
        for (String subPath : subPaths) {
            result = new File(result, subPath);
        }
        return result.getAbsoluteFile();
    }

    @NotNull
    static File getUserHomeDir() {
        String path = System.getProperty("user.home");
        if (isEmpty(path)) throw new IllegalStateException("user.home property is empty");
        return new File(path).getAbsoluteFile();
    }

    @NotNull
    static File userHomeFile(@NotNull String path, @NotNull String... subPaths) {
        File result = getUserHomeDir();
        result = new File(result, path);
        for (String subPath : subPaths) {
            result = new File(result, subPath);
        }
        return result.getAbsoluteFile();
    }

    String RAM_DRIVE_PATH_PROPERTY_KEY = PROJECT_ID + ".ram-drive";

    String RAM_DRIVE_PATH_ENV_VAR_KEY = new String("RAM_DRIVE");

    @NotNull
    static File getRamDriveDir() {
        String path = System.getProperty(RAM_DRIVE_PATH_PROPERTY_KEY);
        if (isEmpty(path)) path = System.getenv(RAM_DRIVE_PATH_ENV_VAR_KEY);
        if (isEmpty(path)) path = userHomeFile(RAM_DRIVE_PATH_PROPERTY_KEY).getPath();
        return new File(path).getAbsoluteFile();
    }

    @NotNull
    static File ramDriveFile(@NotNull String path, @NotNull String... subPaths) {
        File result = getRamDriveDir();
        result = new File(result, path);
        for (String subPath : subPaths) {
            result = new File(result, subPath);
        }
        return result.getAbsoluteFile();
    }

}

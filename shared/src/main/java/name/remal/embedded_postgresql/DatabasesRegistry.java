package name.remal.embedded_postgresql;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.*;

import static name.remal.embedded_postgresql.AtomicFileOperations.readTextFile;
import static name.remal.embedded_postgresql.AtomicFileOperations.writeTextFile;
import static name.remal.embedded_postgresql.Constants.DATABASES_REGISTRY_FILE;

abstract class DatabasesRegistry {

    private static final char DELIM = ';';

    @NotNull
    public static Collection<File> readDataDirs() {
        Collection<File> result = new LinkedHashSet<>();
        for (Item item : readItems()) {
            result.add(item.getDataDir());
        }
        return result;
    }

    @Nullable
    public static Integer readPort(@NotNull File dataDir) {
        return Optional.ofNullable(readItem(dataDir))
            .map(Item::getPort)
            .orElse(null);
    }

    public static void store(@NotNull File dataDir, int port) {
        Collection<Item> items = readItems();
        items.add(new Item(dataDir, port));
        StringBuilder sb = new StringBuilder();
        for (Item item : items) {
            sb.append(item.port).append(DELIM).append(item.dataDir.getPath()).append('\n');
        }
        writeTextFile(DATABASES_REGISTRY_FILE, sb.toString());
    }

    @NotNull
    private static Collection<Item> readItems() {
        Collection<Item> result = new LinkedHashSet<>();
        try (Scanner scanner = new Scanner(readTextFile(DATABASES_REGISTRY_FILE))) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine().trim();
                if (line.isEmpty()) continue;
                int delimPos = line.indexOf(DELIM);
                int port = Integer.parseUnsignedInt(line.substring(0, delimPos));
                String dataDirPath = line.substring(delimPos + 1);
                File curDataDir = new File(dataDirPath);
                result.add(new Item(curDataDir, port));
            }
        }
        return result;
    }

    @Nullable
    private static Item readItem(@NotNull File dataDir) {
        dataDir = dataDir.getAbsoluteFile();
        for (Item item : readItems()) {
            if (Objects.equals(dataDir, item.getDataDir())) {
                return item;
            }
        }
        return null;
    }

    @Data
    @EqualsAndHashCode(of = "dataDir")
    private static class Item {

        @NotNull
        private final File dataDir;

        private final int port;

        public Item(@NotNull File dataDir, int port) {
            this.dataDir = dataDir.getAbsoluteFile();
            this.port = port;
        }

    }

}

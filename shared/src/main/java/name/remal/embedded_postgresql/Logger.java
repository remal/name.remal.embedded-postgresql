package name.remal.embedded_postgresql;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import static name.remal.embedded_postgresql.Utils.isClassExists;
import static org.slf4j.LoggerFactory.getLogger;

interface Logger {

    boolean IS_LOGGER_IN_CLASSPATH = isClassExists("org.slf4j.LoggerFactory");

    String LOGGER_NAME = Logger.class.getName();

    static void logDebug(@NotNull String format, @Nullable Object... args) {
        if (IS_LOGGER_IN_CLASSPATH) getLogger(LOGGER_NAME).debug(format, args);
    }

    static void logInfo(@NotNull String format, @Nullable Object... args) {
        if (IS_LOGGER_IN_CLASSPATH) getLogger(LOGGER_NAME).info(format, args);
    }

    static void logWarn(@NotNull String format, @Nullable Object... args) {
        if (IS_LOGGER_IN_CLASSPATH) getLogger(LOGGER_NAME).warn(format, args);
    }

    static void logError(@NotNull String format, @Nullable Object... args) {
        if (IS_LOGGER_IN_CLASSPATH) getLogger(LOGGER_NAME).error(format, args);
    }

}

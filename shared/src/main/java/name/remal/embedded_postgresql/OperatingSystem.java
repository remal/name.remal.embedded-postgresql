package name.remal.embedded_postgresql;

import org.jetbrains.annotations.NotNull;

import java.util.Optional;

interface OperatingSystem {

    @NotNull
    String getName();

    default boolean isWindows() {
        return "windows".equalsIgnoreCase(getName());
    }

    @NotNull
    String getArch();

    @NotNull
    String getClassifier();

    long getTotalMemory();

    int getPid();

    @NotNull
    default String getProcessPath() {
        return getProcessPath(getPid()).get();
    }

    boolean isAlive(int pid);

    @NotNull
    Optional<String> getProcessPath(int pid);

}

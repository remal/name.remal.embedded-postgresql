package name.remal.embedded_postgresql;

import org.jetbrains.annotations.NotNull;

abstract class DatabaseUtils {

    public static int getDefaultPort() {
        return 5432;
    }

    @NotNull
    public static String escapeObjectName(@NotNull String name) {
        return '"' + name.replace("\"", "\"\"") + '"';
    }

    private DatabaseUtils() {
    }

}

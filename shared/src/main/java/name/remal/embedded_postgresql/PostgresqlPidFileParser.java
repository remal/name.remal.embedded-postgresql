package name.remal.embedded_postgresql;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.List;

import static java.nio.file.Files.readAllLines;
import static java.util.stream.Collectors.toList;

interface PostgresqlPidFileParser {

    String PID_FILE_RELATIVE_PATH = "postmaster.pid";

    @Nullable
    @SneakyThrows
    static PostgresqlPidFileInfo parsePostgresqlPidFile(@NotNull File pidFile) {
        pidFile = pidFile.getAbsoluteFile();
        if (!pidFile.isFile()) return null;

        List<String> lines = readAllLines(pidFile.toPath()).stream().map(String::trim).collect(toList());
        return new PostgresqlPidFileInfo(
            pidFile.getParentFile().getPath(),
            Integer.parseInt(lines.get(0)),
            Integer.parseInt(lines.get(3))
        );
    }

    @Nullable
    static PostgresqlPidFileInfo parsePostgresqlPidFileFromDataDir(@NotNull File dataDir) {
        File pidFile = new File(dataDir, PID_FILE_RELATIVE_PATH);
        return parsePostgresqlPidFile(pidFile);
    }

}

package name.remal.embedded_postgresql;

import org.jetbrains.annotations.NotNull;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.Character.MAX_RADIX;
import static java.lang.Math.abs;
import static java.lang.System.currentTimeMillis;

abstract class ID {

    private static final BigInteger CLASS_HASH_PART = BigInteger.valueOf(abs(ID.class.hashCode()));

    private static final AtomicInteger COUNTER = new AtomicInteger(abs(new SecureRandom().nextInt()));

    private static int incrementAndGetCounter() {
        while (true) {
            int value = COUNTER.incrementAndGet();
            if (0 <= value) return value;
            if (COUNTER.compareAndSet(value, -1)) continue;
        }
    }

    @NotNull
    public static String createId() {
        BigInteger id = BigInteger.valueOf(currentTimeMillis());
        id = id.shiftLeft(Integer.SIZE).or(BigInteger.valueOf(incrementAndGetCounter()));
        id = id.shiftLeft(Integer.SIZE).or(CLASS_HASH_PART);
        return id.toString(MAX_RADIX);
    }

    private ID() {
    }

}

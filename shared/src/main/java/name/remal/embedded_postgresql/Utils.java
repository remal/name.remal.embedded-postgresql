package name.remal.embedded_postgresql;

import lombok.SneakyThrows;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import static java.lang.String.format;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.file.FileVisitResult.CONTINUE;
import static java.nio.file.Files.*;
import static name.remal.embedded_postgresql.FS.newTempFile;
import static name.remal.embedded_postgresql.Logger.logDebug;

abstract class Utils {

    public static final Charset CHARSET = UTF_8;

    @NotNull
    public static String defaultString(@Nullable CharSequence value, @NotNull String defaultValue) {
        return isNotEmpty(value) ? value.toString() : defaultValue;
    }

    @NotNull
    public static String defaultString(@Nullable CharSequence value) {
        return defaultString(value, "");
    }

    @Contract(value = "null->true", pure = true)
    public static boolean isEmpty(@Nullable CharSequence value) {
        return null == value || 0 == value.length();
    }

    @Contract(value = "null->false", pure = true)
    public static boolean isNotEmpty(@Nullable CharSequence value) {
        return !isEmpty(value);
    }

    @Contract(value = "null->true", pure = true)
    public static boolean isEmpty(@Nullable Collection value) {
        return null == value || 0 == value.size();
    }

    @Contract(value = "null->false", pure = true)
    public static boolean isNotEmpty(@Nullable Collection value) {
        return !isEmpty(value);
    }

    @SneakyThrows
    public static void deleteRecursive(@NotNull Path path) {
        if (!exists(path)) return;
        walkFileTree(path, new SimpleFileVisitor<Path>() {
            @NotNull
            @Override
            public FileVisitResult visitFile(final Path file, final BasicFileAttributes attrs) throws IOException {
                deleteIfExists(file);
                return CONTINUE;
            }

            @NotNull
            @Override
            public FileVisitResult postVisitDirectory(final Path dir, @Nullable final IOException e) throws IOException {
                if (null != e) throw e;
                deleteIfExists(dir);
                return CONTINUE;
            }
        });
    }

    public static void deleteRecursive(@NotNull File file) {
        deleteRecursive(file.toPath());
    }

    public static boolean isClassExists(@NotNull String className) {
        try {
            Class.forName(className);
            return true;
        } catch (ClassNotFoundException ignored) {
            return false;
        }
    }

    @NotNull
    @SneakyThrows
    public static String decodeURIComponent(@NotNull String uriComponent) {
        return URLDecoder.decode(uriComponent, CHARSET.name());
    }

    @NotNull
    @SneakyThrows
    public static String encodeURIComponent(@NotNull String uriComponent) {
        return URLEncoder.encode(uriComponent, CHARSET.name());
    }

    @NotNull
    @SneakyThrows
    public static String executeCommand(@NotNull String command, @NotNull String... args) {
        String[] processCommand = new String[1 + args.length];
        processCommand[0] = command;
        System.arraycopy(args, 0, processCommand, 1, args.length);

        File outputFile = newTempFile("command", "output");
        File errorOutputFile = newTempFile("command", "error");

        logDebug("Executing {} with output file {} and error file {}", command, outputFile, errorOutputFile);

        Process process = new ProcessBuilder(processCommand)
            .redirectOutput(outputFile)
            .redirectError(errorOutputFile)
            .start();
        int exitCode = process.waitFor();

        if (0 != exitCode) {
            throw new CommandExecutingException(
                format("Error executing command %s:%n%nOutput file: %s%nError output file: %s", command, outputFile, errorOutputFile),
                outputFile,
                errorOutputFile
            );
        }

        byte[] resultBytes = readAllBytes(outputFile.toPath());
        String result = new String(resultBytes, UTF_8);

        outputFile.delete();
        errorOutputFile.delete();

        return result;
    }

    @NotNull
    public static Set<Throwable> getThrowableHierarchy(@NotNull Throwable throwable) {
        Set<Throwable> hierarchy = new LinkedHashSet<>();
        fillThrowableHierarchy(hierarchy, throwable);
        return hierarchy;
    }

    @SuppressWarnings("unchecked")
    private static void fillThrowableHierarchy(@NotNull Set<Throwable> hierarchy, @NotNull Throwable throwable) {
        if (throwable instanceof Iterable) {
            ((Iterable<Throwable>) throwable).forEach(t -> fillThrowableHierarchy(hierarchy, t));
        } else {
            if (!hierarchy.add(throwable)) return;
            Throwable cause = throwable.getCause();
            if (null != cause) fillThrowableHierarchy(hierarchy, cause);
            for (Throwable suppressed : throwable.getSuppressed()) {
                fillThrowableHierarchy(hierarchy, suppressed);
            }
        }
    }

    private Utils() {
    }

}

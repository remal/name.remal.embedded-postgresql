package name.remal.embedded_postgresql;

import java.io.File;

import static name.remal.embedded_postgresql.BuildInfo.POSTGRESQL_VERSION;
import static name.remal.embedded_postgresql.BuildInfo.PROJECT_ID;
import static name.remal.embedded_postgresql.FS.file;
import static name.remal.embedded_postgresql.FS.userHomeFile;

interface Constants {

    String ADMIN_USER = new String("postgres");

    String ADMIN_DATABASE = new String("postgres");

    File ROOT_DIR = userHomeFile(PROJECT_ID);

    File BASE_VERSION_DIR = file(ROOT_DIR, POSTGRESQL_VERSION);

    File BINARIES_DIR = file(BASE_VERSION_DIR, "binaries");

    String DATA_RELATIVE_PATH = "data";

    File DATABASES_REGISTRY_FILE = file(ROOT_DIR, "databases-registry");

    File DATA_DIR = file(ROOT_DIR, DATA_RELATIVE_PATH);

    String DATA_PROJECT_RELATIVE_PATH = '.' + PROJECT_ID;

}

package name.remal.embedded_postgresql;

import lombok.SneakyThrows;
import name.remal.embedded_postgresql.Lambda.UncheckedConsumer;
import name.remal.embedded_postgresql.Lambda.UncheckedFunction;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Objects;

import static java.nio.file.Files.createDirectories;
import static java.util.Objects.requireNonNull;
import static name.remal.embedded_postgresql.ID.createId;
import static name.remal.embedded_postgresql.Lambda.UncheckedRunnable;
import static name.remal.embedded_postgresql.Utils.CHARSET;
import static name.remal.embedded_postgresql.Utils.deleteRecursive;

abstract class AtomicFileOperations {

    @SneakyThrows
    public static boolean createNewFile(@NotNull File file) {
        file = file.getAbsoluteFile();
        createDirectories(file.getParentFile().toPath());
        return file.createNewFile();
    }

    @NotNull
    @SneakyThrows
    private static byte[] readFileChannel(@NotNull FileChannel channel) {
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            while (true) {
                ByteBuffer buffer = ByteBuffer.allocate(8192);
                int readBytesCount = channel.read(buffer);
                if (readBytesCount < 0) break;
                outputStream.write(buffer.array(), 0, readBytesCount);
            }
            return outputStream.toByteArray();
        }
    }

    @NotNull
    public static byte[] readFile(@NotNull File file) {
        Reference<byte[]> resultReference = new Reference<>();
        forLockedFile(file, channel -> {
            resultReference.object = readFileChannel(channel);
        });
        return requireNonNull(resultReference.object);
    }

    @SneakyThrows
    public static void writeFile(@NotNull File file, @Nullable byte[] bytes) {
        file = file.getAbsoluteFile();
        createNewFile(file);

        if (null != bytes) {
            forLockedFile(file, channel -> {
                channel.write(ByteBuffer.wrap(bytes));
            });
        }
    }

    public static void modifyFile(@NotNull File file, @NotNull UncheckedFunction<byte[], byte[]> action) {
        createNewFile(file);
        forLockedFile(file, channel -> {
            byte[] source = readFileChannel(channel);
            byte[] result = action.apply(source);
            if (null != result && !Arrays.equals(source, result)) {
                channel.position(0).truncate(result.length).write(ByteBuffer.wrap(result));
            }
        });
    }

    @NotNull
    public static String readTextFile(@NotNull File file, @Nullable Charset charset) {
        Reference<String> resultReference = new Reference<>();
        forLockedFile(file, channel -> {
            byte[] bytes = readFileChannel(channel);
            resultReference.object = new String(bytes, null != charset ? charset : Charset.defaultCharset());
        });
        return requireNonNull(resultReference.object);
    }

    @NotNull
    public static String readTextFile(@NotNull File file) {
        return readTextFile(file, CHARSET);
    }

    @SneakyThrows
    public static void writeTextFile(@NotNull File file, @Nullable String text, @Nullable Charset charset) {
        file = file.getAbsoluteFile();
        createNewFile(file);

        if (null != text) {
            forLockedFile(file, channel -> {
                byte[] bytes = text.getBytes(null != charset ? charset : Charset.defaultCharset());
                channel.position(0).truncate(bytes.length).write(ByteBuffer.wrap(bytes));
            });
        }
    }

    public static void writeTextFile(@NotNull File file, @Nullable String text) {
        writeTextFile(file, text, CHARSET);
    }

    public static void modifyTextFile(@NotNull File file, @Nullable Charset charset, @NotNull UncheckedFunction<String, String> action) {
        createNewFile(file);
        forLockedFile(file, channel -> {
            String source = new String(readFileChannel(channel), null != charset ? charset : Charset.defaultCharset());
            String result = action.apply(source);
            if (null != result && !Objects.equals(source, result)) {
                byte[] bytes = result.getBytes(null != charset ? charset : Charset.defaultCharset());
                channel.position(0).truncate(bytes.length).write(ByteBuffer.wrap(bytes));
            }
        });
    }

    public static void modifyTextFile(@NotNull File file, @NotNull UncheckedFunction<String, String> action) {
        modifyTextFile(file, CHARSET, action);
    }

    @SneakyThrows
    public static void forLockedFile(@NotNull File file, @NotNull UncheckedConsumer<FileChannel> action) {
        file = file.getAbsoluteFile();
        createDirectories(file.getParentFile().toPath());
        int attemptNumber = 0;
        while (true) {
            ++attemptNumber;
            try (FileChannel channel = new RandomAccessFile(file, "rws").getChannel()) {
                final FileLock fileLock;
                try {
                    fileLock = channel.lock();
                } catch (@NotNull ClosedChannelException | FileLockInterruptionException | OverlappingFileLockException | NonWritableChannelException e) {
                    if (1000 <= attemptNumber) throw e;
                    Thread.sleep(50);
                    continue;
                }

                try {
                    action.accept(channel);
                    return;

                } finally {
                    fileLock.release();
                }
            }
        }
    }

    public static void forLockedFile(@NotNull File file, @NotNull UncheckedRunnable action) {
        forLockedFile(file, __ -> action.run());
    }

    public static void synchronizedWithTempFile(@NotNull File file, @NotNull UncheckedRunnable action) {
        forLockedFile(file, action);
        file.delete();
    }

    @SneakyThrows
    public static boolean createDirIfNotExists(@NotNull File dir, @NotNull UncheckedConsumer<File> tempDirConsumer) {
        dir = dir.getAbsoluteFile();
        if (dir.exists()) {
            if (dir.isDirectory()) {
                return false;
            } else {
                throw new IllegalArgumentException(dir + " exists and not a directory");
            }
        }

        File tempDir = new File(dir.getParentFile(), dir.getName() + ".creating-" + createId());
        deleteRecursive(tempDir);
        createDirectories(tempDir.getParentFile().toPath());
        tempDirConsumer.accept(tempDir);

        createDirectories(dir.getParentFile().toPath());
        boolean result = tempDir.renameTo(dir);

        deleteRecursive(tempDir);

        return result;
    }

    private AtomicFileOperations() {
    }

}

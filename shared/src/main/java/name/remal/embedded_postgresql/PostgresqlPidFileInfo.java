package name.remal.embedded_postgresql;

import lombok.Data;
import org.jetbrains.annotations.NotNull;

@Data
class PostgresqlPidFileInfo {

    @NotNull
    private final String dataDirPath;

    private final int pid;

    private final int port;

}

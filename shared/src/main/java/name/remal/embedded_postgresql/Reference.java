package name.remal.embedded_postgresql;

import org.jetbrains.annotations.Nullable;

class Reference<T> {

    @Nullable
    volatile T object;

}

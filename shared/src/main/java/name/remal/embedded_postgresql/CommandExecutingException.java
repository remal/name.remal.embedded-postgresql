package name.remal.embedded_postgresql;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.nio.charset.Charset;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.file.Files.readAllBytes;

class CommandExecutingException extends RuntimeException {

    @Nullable
    private File outputFile;

    @Nullable
    private File errorOutputFile;

    public CommandExecutingException(String message) {
        super(message);
    }

    public CommandExecutingException(String message, Throwable cause) {
        super(message, cause);
    }

    public CommandExecutingException(Throwable cause) {
        super(cause);
    }

    public CommandExecutingException(String message, @Nullable File outputFile, @Nullable File errorOutputFile) {
        super(message);
        this.outputFile = outputFile;
        this.errorOutputFile = errorOutputFile;
    }

    public CommandExecutingException(String message, Throwable cause, @Nullable File outputFile, @Nullable File errorOutputFile) {
        super(message, cause);
        this.outputFile = outputFile;
        this.errorOutputFile = errorOutputFile;
    }

    public CommandExecutingException(Throwable cause, @Nullable File outputFile, @Nullable File errorOutputFile) {
        super(cause);
        this.outputFile = outputFile;
        this.errorOutputFile = errorOutputFile;
    }

    @Nullable
    public File getOutputFile() {
        return errorOutputFile;
    }

    @Nullable
    public String getOutputText() {
        return getOutputText(UTF_8);
    }

    @Nullable
    @SneakyThrows
    public String getOutputText(@NotNull Charset charset) {
        File outputFile = this.outputFile;
        if (null == outputFile) return null;
        byte[] bytes = readAllBytes(outputFile.toPath());
        return new String(bytes, charset);
    }

    @Nullable
    public File getErrorOutputFile() {
        return errorOutputFile;
    }

    @Nullable
    public String getErrorOutputText() {
        return getErrorOutputText(UTF_8);
    }

    @Nullable
    @SneakyThrows
    public String getErrorOutputText(@NotNull Charset charset) {
        File errorOutputFile = this.errorOutputFile;
        if (null == errorOutputFile) return null;
        byte[] bytes = readAllBytes(errorOutputFile.toPath());
        return new String(bytes, charset);
    }

}

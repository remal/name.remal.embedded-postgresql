package name.remal.embedded_postgresql;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Properties;

import static name.remal.embedded_postgresql.BuildInfo.POSTGRESQL_VERSION;
import static org.postgresql.PGProperty.*;

interface PostgresqlDriverUtils {

    @NotNull
    static Properties processProperties(@Nullable Properties props) {
        Properties result = new Properties();

        if (null != props) {
            props.stringPropertyNames().forEach(key -> {
                if (PG_HOST.getName().equals(key)) return;
                if (PG_PORT.getName().equals(key)) return;
                if (PG_DBNAME.getName().equals(key)) return;
                String value = props.getProperty(key);
                if (null != value) result.put(key, value);
            });
        }

        CONNECT_TIMEOUT.set(result, 1);
        LOGIN_TIMEOUT.set(result, 1);

        ASSUME_MIN_SERVER_VERSION.set(result, POSTGRESQL_VERSION);

        return result;
    }

}
